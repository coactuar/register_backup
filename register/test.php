<?php 
include('commons/header.php');
//require ('functions/reg.php');
// if (empty($_GET['title'])){
//     $title ='';
// 	$varified='0';
// }else{
//     $title =$_GET['title'];
// 	$varified='1';
// }

// if (empty($_GET['first_name'])){
//     $fname ='';
// }else{
//     $fname =$_GET['first_name'];
// }

// if (empty($_GET['last_name'])){
//     $lname ='';
// }else{
//     $lname =$_GET['last_name'];
// }

// if (empty($_GET['email_id'])){
//     $email ='';
// }else{
//     $email =$_GET['email_id'];
// }

// if (empty($_GET['phone_no'])){
//     $phone ='';
// }else{
//     $phone =$_GET['phone_no'];
// }

// if (empty($_GET['speciality'])){
//     $specialty ='';
// }else{
//     $specialty =$_GET['speciality'];
// }

// if (empty($_GET['education'])){
//     $education ='';
// }else{
//     $education =$_GET['education'];
// }

// if (empty($_GET['areaofintrest'])){
//     $aoi ='';
// }else{
//     $aoi =$_GET['areaofintrest'];
// }








?>
<!doctype html>
<html>
<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
<style>
@import url('https://fonts.googleapis.com/css?family=Poppins:400,500,600,700&display=swap');
*{
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  font-family: 'Poppins', sans-serif;
}
html,body{
  display: grid;
  height: 100%;
  width: 100%;
  /* place-items: center; */
  background:#101028;
}
::selection{
  background: #fa4299;
  color: #fff;
}
.wrapper{
  overflow: hidden;
  max-width: 350px;
  /* max-height: 500px; */
  /* background: #fff; */
  padding: 10px;
  /* border-radius: 5px; */
  box-shadow: 0px 15px 20px rgba(0,0,0,0.1);
}
.wrapper .title-text{
  display: flex;
  width: 200%;
}
.wrapper .title{
  width: 50%;
  font-size: 35px;
  font-weight: 600;
  text-align: center;
  transition: all 0.6s cubic-bezier(0.68,-0.55,0.265,1.55);
}
.wrapper .slide-controls{
  position: relative;
  display: flex;
  height: 50px;
  width: 100%;
  overflow: hidden;
  margin: 30px 0 10px 0;
  justify-content: space-between;
  border: 1px solid lightgrey;
  /* border-radius: 5px; */
}
.slide-controls .slide{
  height: 100%;
  width: 100%;
  color: #fff;
  font-size: 18px;
  font-weight: 500;
  text-align: center;
  line-height: 48px;
  cursor: pointer;
  z-index: 1;
  transition: all 0.6s ease;
}
/* .slide-controls label.signup{
  color: #000;
} */
.slide-controls .slider-tab{
  position: absolute;
  height: 100%;
  width: 50%;
  left: 0;
  z-index: 0;
  /* border-radius: 5px; */
  background:#004659;
  transition: all 0.6s cubic-bezier(0.68,-0.55,0.265,1.55);
}
input[type="radio"]{
  display: none;
}
#signup:checked ~ .slider-tab{
  left: 50%;
}
#signup:checked ~ label.signup{
  color: #fff;
  cursor: default;
  user-select: none;
}
/* #signup:checked ~ label.login{
  color: #000;
}
#login:checked ~ label.signup{
  color: #000;
} */
#login:checked ~ label.login{
  cursor: default;
  user-select: none;
}
.wrapper .form-container{
  width: 100%;
  overflow: hidden;
}
.form-container .form-inner{
  display: flex;
  width: 200%;
}
.form-container .form-inner form{
  width: 50%;
  transition: all 0.6s cubic-bezier(0.68,-0.55,0.265,1.55);
}
.form-inner form .field{
  height: 50px;
  width: 100%;
  margin-top: 20px;
}
.form-select{
    height: 100%;
  width: 100%;
  outline: none;
  padding-left: 15px;
  border-radius: 5px;
  border: 1px solid lightgrey;
  border-bottom-width: 2px;
  font-size: 17px;
  transition: all 0.3s ease;
}
.form-inner form .field input {
  height: 100%;
  width: 100%;
  outline: none;
  padding-left: 15px;
  /* border-radius: 5px; */
  border: 1px solid lightgrey;
  border-bottom-width: 2px;
  font-size: 17px;
  transition: all 0.3s ease;
}
.form-inner form .field input:focus{
  border-color: #fc83bb;
  /* box-shadow: inset 0 0 3px #fb6aae; */
}
.form-inner form .field input::placeholder{
  color: #999;
  transition: all 0.3s ease;
}
form .field input:focus::placeholder{
  color: #b3b3b3;
}
.form-inner form .pass-link{
  margin-top: 5px;
}
.form-inner form .signup-link{
  text-align: center;
  margin-top: 30px;
}
.form-inner form .pass-link a,
.form-inner form .signup-link a{
  color: #fa4299;
  text-decoration: none;
}
.form-inner form .pass-link a:hover,
.form-inner form .signup-link a:hover{
  text-decoration: underline;
}
form .btn{
  height: 50px;
  width: 100%;
  /* border-radius: 5px; */
  position: relative;
  overflow: hidden;
}
form .btn .btn-layer{
  height: 100%;
  width: 300%;
  position: absolute;
  left: -100%;
  background: #004659;
  /* border-radius: 5px; */
  transition: all 0.4s ease;;
}
form .btn:hover .btn-layer{
  left: 0;
}
form .btn input[type="submit"]{
  height: 100%;
  width: 100%;
  z-index: 1;
  position: relative;
  background: none;
  border: none;
  color: #fff;
  padding-left: 0;
  /* border-radius: 5px; */
  font-size: 20px;
  font-weight: 500;
  cursor: pointer;
}
</style>
<body >
<div class="container-fluid">
          <div class="row">
          <div class="col-12 col-md-4">
<img src="assects/img/Registration.png" class="img-fluid w-50 mt-2 ml-2" alt="" srcset="">
<img src="assects/img/Registration1.png" class="img-fluid w-75 mt-4" alt="" srcset="">

          <div class="wrapper ml-4">
  <!-- <div class="title-text">
    <div class="title login">
    Registration  Employee
    </div>
    <div class="title signup">
    Registration Family
    </div>
  </div> -->
  <div class="form-container">
    <div class="slide-controls">
      <input type="radio" name="slide" id="login" checked>
      <input type="radio" name="slide" id="signup">
      <label for="login" class="slide login">Employee</label>
      <label for="signup" class="slide signup">Family</label>
      <div class="slider-tab"></div>
    </div>
<div class="form-inner">
                 
                  <form method="POST" id="reg-form" class="login">
				 
                  <input type="hidden" id="app" name="app" value="<?= $varified; ?>"> 
                  <div class="field">
   
          <select class="form-select"  name="select" aria-label="Default select example" required>
  <option value="" selected>Zone </option>
  <option value="East">East</option>
  <option value="west">west</option>
  <option value="south">south</option>
  <option value="North">North</option>
</select>
        </div>
        <div class="field">
          <input type="text" placeholder="Name" name="name" required>
        </div>
        <div class="field">
          <input type="text" placeholder="Surname" name="surname" required>
        </div>
        <div class="field">
          <input type="email" placeholder="Email" name="email" required>
        </div>
        <div class="field btn">
          <div class="btn-layer"></div>
          <input type="submit" name="reguser-btn" id="btnSubmit" class="form-submit  "  value="Submit" />
        </div>
 </form>
 <form action="post" id="reg-form1" class="signup">
 
 <input type="hidden" id="app" name="app1" value="<?= $varified; ?>"> 
        <div class="field">
        <select class="form-select"  name="selectemp"  aria-label="Default select example" required>
  <option value="" >Zone </option>
  <option value="East">East</option>
  <option value="west">west</option>
  <option value="south">south</option>
  <option value="North">North</option>
</select>
        </div>
        <div class="field">
          <input type="text" placeholder="Name" name="name1" required>
        </div>
        <div class="field">
          <input type="text" placeholder="Surname" name="surname1" required>
        </div>
        <div class="field">
          <input type="email" placeholder="Email" name="email1" required>
        </div>
        <div class="field">
          <input type="email" placeholder="Employee Email ID " name="emp_email"  required>
        </div>
        <div class="field">
          <input type="text" placeholder="Relationship with Employee" name="relation" required>
        </div>
        <div class="field btn">
          <div class="btn-layer"></div>
          <input type="submit" name="reguser-btn" id="btnSubmit" class="form-submit  "  value="Submit" />
        </div>
      </form>
                </div>
               
                </div>
  </div>

          </div>
          <div class="col-12 col-md-8">
          <img src="assects/img/25.jpg" class="vh-100" width="100%"  alt="" srcset="">
          </div>
          </div>
      </div>    
                 

<div id="code"></div>

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
   <h4> <div id="login-message"></div></h4>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>

<script src="assects/js/jquery.min.js"></script>
<script src="assects/js/bootstrap.min.js"></script>
<script>

const loginText = document.querySelector(".title-text .login");
const loginForm = document.querySelector("form.login");
const loginBtn = document.querySelector("label.login");
const signupBtn = document.querySelector("label.signup");
const signupLink = document.querySelector("form .signup-link a");
signupBtn.onclick = (()=>{
  loginForm.style.marginLeft = "-50%";
  loginText.style.marginLeft = "-50%";
});
loginBtn.onclick = (()=>{
  loginForm.style.marginLeft = "0%";
  loginText.style.marginLeft = "0%";
});
signupLink.onclick = (()=>{
  signupBtn.click();
  return false;
});</script>
<script>
$(document).ready(function() {
    // alert($("#country").val());
    // alert($("#country").trigger("change").val());
    // var conceptName = $('#country').val();
    // alert(conceptName);
});

$(document).on('submit', '#reg-form', function()
{  
  $.post('functions/reg.php', $(this).serialize(), function(data)
  {
      console.log(data);
      if(data == 's')
      {
        $('#login-message').text('You are registered succesfully  Please check your email regarding event Details.');
        $('#login-message').addClass('alert-success');
		$("#exampleModalCenter").modal('show');
		 $('#btnSubmit').fadeOut(); 
		   $('#btnSubmit').delay(5000).fadeIn();
        
          return false;
      }
      else if (data == '1')
      {
          $('#login-message').text('You are already registered.');
          $('#login-message').addClass('alert-danger');
		$("#exampleModalCenter").modal('show');
         
		   $('#btnSubmit').fadeOut(); 
		     $('#btnSubmit').delay(5000).fadeIn();
          return false;
      }
      else
      {

          $('#login-message').text(data);
          $('#login-message').addClass('alert-danger');
		  $("#exampleModalCenter").modal('show');  
		  $('#btnSubmit').fadeOut(); 	  
		  $('#btnSubmit').delay(5000).fadeIn(); 	  
          return false;
      }
  });
  
  return false;
});
$(document).on('submit', '#reg-form1', function()
{  
  $.post('functions/reg1.php', $(this).serialize(), function(data)
  {
      console.log(data);
      if(data == 's')
      {
        $('#login-message').text('You are registered succesfully  Please check your email regarding event Details.');
        $('#login-message').addClass('alert-success');
		$("#exampleModalCenter").modal('show');
		 $('#btnSubmit').fadeOut(); 
		   $('#btnSubmit').delay(5000).fadeIn();
        
          return false;
      }
      else if (data == '1')
      {
          $('#login-message').text('You are already registered.');
          $('#login-message').addClass('alert-danger');
		$("#exampleModalCenter").modal('show');
         
		   $('#btnSubmit').fadeOut(); 
		     $('#btnSubmit').delay(5000).fadeIn();
          return false;
      }
      else
      {

          $('#login-message').text(data);
          $('#login-message').addClass('alert-danger');
		  $("#exampleModalCenter").modal('show');  
		  $('#btnSubmit').fadeOut(); 	  
		  $('#btnSubmit').delay(5000).fadeIn(); 	  
          return false;
      }
  });
  
  return false;
});
</script>

<script>
function getCountries()
{
    $.ajax({
        url: 'functions/server.php',
        data: {action: 'getcountries'},
        type: 'post',
        success: function(response) {
            
            $("#countries").html(response);
            $("#country").trigger("change");
        }
    });
   
}

function updateState()
{
    var c = $('#country').val();
    if(c!='0'){
        $.ajax({
            url: 'functions/server.php',
            data: {action: 'getstates', country : c },
            type: 'post',
            success: function(response) {
                
                $("#states").html(response);
            }
        });
    }
}

function updateCity()
{
    var s = $('#state').val();
    if(s!='0'){
        $.ajax({
            url: 'functions/server.php',
            data: {action: 'getcities', state : s },
            type: 'post',
            success: function(response) {
                
                $("#cities").html(response);
            }
        });
    }
}

getCountries();
//updateState();
</script>


</body>
</html>